﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.BaseService.Monitor.Dal;
using BSF.BaseService.Monitor.Model;
using BSF.Db;
using BSF.Tool;
using BSF.BaseService.Monitor.Base.Entity;


namespace BSF.BaseService.Monitor.SystemRuntime.BatchQueues
{
    public class LogBatchQueue : BaseBatchQueue<CommonLogInfo>
    {
        public override int MaxQueueCount { get { return 10000; } }

        public override int MaxSleepTime { get { return 5000 * 60; } }//5分钟

        public override int SleepTime { get { return 5000; } }

        public override string BatchTable { get { return "tb_log"; } }

        protected override void BatchCommit()
        {
            if (!string.IsNullOrEmpty(Config.UnityLogConnectString))
            {
                if (TempQueue.Count > 500)
                {
                    var dict = new Dictionary<string, string>
                            {
                                {"logcreatetime","logcreatetime"},
                                {"logtype", "logtype"},
                                {"projectname", "projectname"},
                                {"logtag", "logtag"},
                                {"msg", "msg"}
                            };
                    var timewatchinfoTable = DataTableHelper.ConvertToDataTable<CommonLogInfo>(TempQueue);
                    using (var c = DbConn.CreateConn(Db.DbType.SQLSERVER, Config.UnityLogConnectString))
                    {
                        c.Open();
                        //c.BeginTransaction();
                        try
                        {
                            c.SqlBulkCopy(timewatchinfoTable, BatchTable + DateTime.Now.ToString("yyyyMM"), "", new List<ProcedureParameter>(), dict, 0);
                            //c.Commit();
                        }
                        catch (Exception exp)
                        {
                            //c.Rollback();
                        }

                    }
                }
                else
                {
                    foreach (var t in TempQueue)
                    {
                        try
                        {
                            SqlHelper.ExcuteSql(Config.UnityLogConnectString, (c) =>
                            {
                                tb_log_dal logdal = new tb_log_dal();
                                logdal.Add(c, new tb_log_model()
                                {
                                    logcreatetime = t.logcreatetime,
                                    logtag = t.logtag,
                                    logtype = t.logtype,
                                    msg = t.msg,
                                    projectname = t.projectname,
                                    sqlservercreatetime = t.sqlservercreatetime
                                });
                            });
                        }
                        catch { }
                    }
                }
            }
        }
    }
}
